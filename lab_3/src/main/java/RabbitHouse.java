import java.util.Scanner;

public class RabbitHouse{
  public static int ans = 0,faktorial = 1,answer = 0;
  public static void solve(int jumlah){
    int hitung;
    if(jumlah == 0){
      return;
    } else{
      ans += faktorial;
      faktorial = faktorial * jumlah;
      solve(jumlah-1);
      return;
    }
  }

  public static boolean cekPalindrom(String input){
    for(int i=0;i<input.length()/2;i++) if(input.charAt(i) != input.charAt(input.length()-i-1)) return false;
    return true;
  }

  public static int Solve(String input){
    if(input.length() == 0) return 0;
    if(cekPalindrom(input)) return 0;
    int hitung = 1;
    for(int i=0;i<input.length();i++){
      String temp="";
      for(int j=0;j<input.length();j++) if(i != j) temp = temp + input.charAt(j);
      hitung += Solve(temp);
    }
    return hitung;
  }
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    String in = input.nextLine();
    String arr[] = in.split(" ");
    String jenis =arr[0],namaKelinci = arr[1];
    if(jenis.equals("normal")){
      solve(namaKelinci.length());
      System.out.println(ans);
      ans = 0;
      faktorial = 1;
      answer = 0;
    }
    else
    System.out.println(Solve(namaKelinci));
  }
}
