package xoxo.crypto;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) throws KeyTooLongException {
      if(hugKeyString.length() > 28){
        throw new KeyTooLongException("Key too long");
      }
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) throws RangeExceededException{

        //TODO: Implement decryption algorithm
        System.out.println(seed);
        if(seed < 0 || seed > 36){
        //  System.out.println("tes");
          throw new RangeExceededException("Range exceeded");
        }
        String message = "";
      //  System.out.println("encryptedMessage");
        for(int i = 0;i < encryptedMessage.length(); i++){
    //        System.out.println(encryptedMessage.charAt(i) + " " + i);
            message += (char)(encryptedMessage.charAt(i)^((hugKeyString.charAt(i % hugKeyString.length())^seed)-'a'));
        }
        return message;
    }
}
