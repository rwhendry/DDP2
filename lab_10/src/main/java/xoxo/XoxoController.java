package xoxo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;


/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

  /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(decryptListener);
        gui.setEncryptFunction(encryptListener);
        //TODO: Write your code for logic and everything here
    }

    //TODO: Create any methods that you want

    private ActionListener decryptListener = new ActionListener() {

        public void actionPerformed(ActionEvent e){
          try{
            XoxoDecryption xoxoDecryption = new XoxoDecryption(gui.getKeyText());
            int seed;
          //  System.out.println(gui.getSeedText());
            if(gui.getSeedText().equals("")){
              seed = 18;
            }
            else seed = Integer.parseInt(gui.getSeedText());
            String result = xoxoDecryption.decrypt(gui.getMessageText(),seed);
            //System.out.println(seed + " " + result);
            gui.appendLog(result);
            //System.out.println(seed + " " + result);
            //throw new RuntimeException("aw");
          } catch(Exception ex){
            JOptionPane.showMessageDialog(null,ex.getMessage());
          }
        }
    };

    private ActionListener encryptListener = new ActionListener() {


        public void actionPerformed(ActionEvent e) {
            try {
                XoxoEncryption XoxoEncryption = new XoxoEncryption(gui.getKeyText());
                String result;
                if (gui.getSeedText().equals("")){
                    result = XoxoEncryption.encrypt(gui.getMessageText()).getEncryptedMessage();
                }
                else{
                    int seed = Integer.parseInt(gui.getSeedText());
                    result = XoxoEncryption.encrypt(gui.getMessageText(),seed).getEncryptedMessage();
                }
                gui.appendLog(result);
            } catch (Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    };

}
