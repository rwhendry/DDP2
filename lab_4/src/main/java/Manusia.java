public class Manusia{
  private String nama;
  private int uang,umur;
  private float kebahagiaan;
  static Manusia manusiaTerakhir;
  private Boolean status;

  public Manusia(String nama,int umur,int uang){
    this.nama = nama;
    this.umur = umur;
    this.uang = uang;
    this.kebahagiaan = 50;
    this.status = false;
    manusiaTerakhir = this;
  }
  public Manusia(String nama,int umur){
    this.nama = nama;
    this.umur = umur;
    this.uang = 50000;
    this.kebahagiaan = 50;
    this.status = false;
    manusiaTerakhir = this;
  }

  public void beriUang(Manusia penerima){
    if(this.status){
      System.out.println(this.nama + " telah tiada");
      return;
    }
    if(penerima.status){
      System.out.println(penerima.nama + " telah tiada");
      return;
    }
    int uang = 0;
    String namaPenerima = penerima.getNama();
    for(int i=0;i<namaPenerima.length();i++){
      int asc = (int) namaPenerima.charAt(i);
      uang += asc;
    }
    uang *= 100;
    int uangPenerima = penerima.getUang();
    if(this.uang < uang){
      System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima + "namun tidak memiliki cukup uang :'(");
    }
    else{
      this.uang -= uang;
      uangPenerima += uang;
      this.kebahagiaan += ((float) uang/6000);
      if(this.kebahagiaan > 100){
        this.kebahagiaan = 100;
      }
      penerima.setUang(uangPenerima);
      float kebahagiaanPenerima = penerima.getKebahagiaan();
      kebahagiaanPenerima += ((float)uang/6000);
      if(kebahagiaanPenerima > 100) {
        kebahagiaanPenerima = 100;
      }

      penerima.setKebahagiaan(kebahagiaanPenerima);
      System.out.println (this.nama + " memberi uang sebanyak "+uang+" kepada "+namaPenerima+", mereka berdua senang :D");
    }
  }

  public void beriUang(Manusia penerima,int uang){
    if(this.status){
      System.out.println(this.nama + " telah tiada");
      return;
    }
    if(penerima.status){
      System.out.println(penerima.nama + " telah tiada");
      return;
    }
    String namaPenerima = penerima.getNama();
    int uangPenerima = penerima.getUang();
    if(this.uang < uang){
      System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima + "namun tidak memiliki cukup uang :'(");
    }
    else{
      this.uang -= uang;
      this.kebahagiaan += ((float) uang/6000);
      if(this.kebahagiaan > 100){
        this.kebahagiaan = 100;
      }
      uangPenerima += uang;
      penerima.setUang(uangPenerima);
      float kebahagiaanPenerima = penerima.getKebahagiaan();
      kebahagiaanPenerima += ((float)uang/6000);
      if(kebahagiaanPenerima > 100) {
        kebahagiaanPenerima = 100;
      }
      penerima.setKebahagiaan(kebahagiaanPenerima);
      System.out.println (this.nama + " memberi uang sebanyak "+uang+" kepada "+namaPenerima+", mereka berdua senang :D");
    }
  }

  public void bekerja(int durasi,int bebanKerja){
    if(this.status){
      System.out.println(this.nama + " telah tiada");
      return;
    }
    int umur = this.umur;
    String nama = this.nama;
    float kebahagiaan = this.kebahagiaan;
    if(umur < 18){
      System.out.println(nama+" belum boleh bekerja karena masih dibawah umur D:");
      return;
    }
    int bebanKerjaTotal = durasi * bebanKerja;
    int pendapatan;
    if(bebanKerjaTotal <= kebahagiaan){
      kebahagiaan -= bebanKerjaTotal;
      pendapatan = bebanKerjaTotal * 10000;
      System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
    }
    else{
      int durasiBaru = (int)(kebahagiaan/bebanKerja);
      bebanKerjaTotal = durasiBaru * bebanKerja;
      pendapatan = bebanKerjaTotal * 10000;
      kebahagiaan -= bebanKerjaTotal;
      System.out.println(nama+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+pendapatan);
    }
    this.uang += pendapatan;
    this.kebahagiaan = kebahagiaan;

  }

  public void rekreasi(String namaTempat){
    if(this.status){
      System.out.println(this.nama + " telah tiada");
      return;
    }
    int biaya = namaTempat.length() * 10000;
    if(this.uang >= biaya){
      this.uang -= biaya;
      this.kebahagiaan += namaTempat.length();
      if(this.kebahagiaan > 100) {
        this.kebahagiaan = 100;
      }
      System.out.println(this.nama+" berekreasi di "+namaTempat+", "+this.nama+" senang :)");
    }
    else{
      System.out.println(this.nama+" tidak mempunyai cukup uang untuk berekreasi di "+namaTempat+" :(");
    }
  }

  public void sakit(String namaPenyakit){
    if(this.status){
      System.out.println(this.nama + " telah tiada");
      return;
    }
    this.kebahagiaan -= namaPenyakit.length();
    if(this.kebahagiaan < 0) {
      this.kebahagiaan = 0;
    }
    System.out.println(this.nama + " terkena penyakit "+namaPenyakit+" :O");
  }

  public void meninggal(){
      if(this.status){
        System.out.println(this.nama + " telah tiada");
        return;
      }
      this.uang = 0;
      System.out.println(this.nama + " meninggal dengan tenang, kebahagiaan : "+this.kebahagiaan);
      this.status = true;
      if(this == manusiaTerakhir || manusiaTerakhir.status){
        System.out.println("Semua harta " + this.nama + " hangus");
      }
      else{
        System.out.println("Semua harta "+this.nama+" telah disumbangkan untuk"+manusiaTerakhir.nama);
        manusiaTerakhir.uang += this.uang;
      }
  }

  public void setUang(int uang){
    this.uang = uang;
  }
  public void setKebahagiaan(float kebahagiaan){
    this.kebahagiaan = kebahagiaan;
  }
  public void setName(String nama){
    this.nama = nama;
  }
  public void setUmur(int umur){
    this.umur = umur;
  }
  public int getUang(){
    return this.uang;
  }
  public float getKebahagiaan(){
    return this.kebahagiaan;
  }
  public String getNama(){
    return this.nama;
  }
  public int getUmur(){
    return this.umur;
  }
  public Boolean getStatus(){
    return this.status;
  }

  public String toString(){
    String temp="";
    if(this.status) {
      temp = "Almarhum ";
    }
    return ("Nama\t\t:"+temp+this.nama+"\nUmur\t\t:"+this.umur+"\nUang\t\t:"+this.uang+"\nKebahagiaan\t:"+this.kebahagiaan);
  }
}
