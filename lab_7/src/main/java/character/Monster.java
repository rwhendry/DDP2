package character;

public class Monster extends Player{
  private String roar;
  public Monster(String name,int hp){
    super(name,hp*2);
    roar = "AAAAAAaaaAAAAAaaaAAAAAA";
  }

  public Monster(String name,int hp,String roar){
    super(name,hp*2);
    this.roar = roar;
  }

  public String roar(){
    return roar;
  }

  public String getType(){
    return "Monster";
  }
}

//  write Monster Class here
