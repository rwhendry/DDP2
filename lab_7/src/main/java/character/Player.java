package character;
import java.util.ArrayList;

public abstract class Player{
  private String name;
  private int hp;
  private Boolean isDead,isBurnt;
  private ArrayList<Player> eaten = new ArrayList<Player>();

  public Player(String name,int hp){
    this.name = name;
    this.hp = hp;
    if(this.hp > 0) isDead = false;
    else {
      this.hp = 0;
      isDead = true;
    }
    this.isBurnt = false;
    eaten.clear();
  }

  public String getName(){
    return name;
  }

  public int getHp(){
    return hp;
  }

  public void setHp(int hp){
    if(hp <= 0){
      isDead = true;
      hp = 0;
    }
    this.hp = hp;
  }

  public Boolean getIsDead(){
      return isDead;
  }

  public Boolean getIsBurnt(){
    return isBurnt;
  }

  public String roar(){
    return "";
  }

  public String burn(Player player2){
    return "";
  }

  public String attack(Player player2){
    int hp2 = player2.getHp();
    String jawaban;
    hp2 -= 10;
    player2.setHp(hp2);
    boolean mati = player2.getIsDead();
    jawaban = "Nyawa "+player2.getName()+" " + hp2;
    return jawaban;
  }

  public void setIsBurnt(Boolean isBurnt){
    this.isBurnt = isBurnt;
  }

  public Boolean canEat(Player player2){
      Boolean mati = player2.getIsDead();
      return mati;
  }

  public String getRoar(){
    return "";
  }

  public void addEaten(Player player){
    eaten.add(player);
  }

  public ArrayList<Player> getEaten(){
    return eaten;
  }

  public String getType(){
    return "";
  }

  public String getAlive(){
    if(isDead){
      return "Sudah meninggal dunia dengan damai";
    }
    else{
      return "Masih hidup";
    }

  }
}
//  write Player Class here
