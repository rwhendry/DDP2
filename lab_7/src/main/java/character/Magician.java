package character;

public class Magician extends Human{

  public Magician(String name,int hp){
    super(name,hp);
  }

  public String getType(){
    return "Magician";
  }

  public String burn(Player player2){
    int hp2 = player2.getHp();
    String jawaban;
    hp2 -= 10;
    if(player2 instanceof Magician){
      hp2-= 10;
    }
    player2.setHp(hp2);
    boolean mati = player2.getIsDead();
    if(mati){
      jawaban = "Nyawa "+player2.getName()+" 0\n dan matang";
      player2.setIsBurnt(true);
    }
    else{
      jawaban = "Nyawa "+player2.getName()+" " + hp2;
    }
    return jawaban;
  }
}
//  write Magician Class here
