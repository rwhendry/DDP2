package character;

public class Human extends Player{

  public Human(String name,int hp){
    super(name,hp);
  }

  public Boolean canEat(Player player2){
    Boolean dead = player2.getIsDead();
    Boolean monster = player2 instanceof Monster;
    Boolean burnt = player2.getIsBurnt();
    return dead & monster & burnt;
  }

  public String getType(){
    return "Human";
  }
}

//  write Human Class here
