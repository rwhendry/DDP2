import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for(Player i : player){
          if(i.getName().equals(name)){
            return i;
          }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        String jawaban;
        Player tmp = find(chara);
        if(tmp == null){
          if(tipe.equals("Monster")){
            tmp = new Monster(chara,hp);
          }
          else if(tipe.equals("Human")){
            tmp = new Human(chara,hp);
          }
          else {
            tmp = new Magician(chara,hp);
          }
          player.add(tmp);
          jawaban = chara + " ditambah ke game";
        }
        else{
          jawaban = "Sudah ada karakter bernama " + chara;
        }
        return jawaban ;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
      String jawaban;
      Player tmp = find(chara);
      if(tmp == null){
        if(tipe.equals("Monster")){
          tmp = new Monster(chara,hp,roar);
        }
        else if(tipe.equals("Human")){
          tmp = new Human(chara,hp);
        }
        else {
          tmp = new Magician(chara,hp);
        }
        player.add(tmp);
        jawaban = chara + " ditambah ke game";
      }
      else{
        jawaban = "Sudah ada karakter bernama " + chara;
      }
      return jawaban ;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player tmp = find(chara);
        if(tmp == null){
          return "Tidak ada " + chara;
        }
        String jawaban;
        if(tmp == null){
          jawaban = "Tidak ada " + chara;
        }
        else{
          player.remove(tmp);
          jawaban = chara + " dihapus dari game";
        }
        return jawaban;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
       String jawaban;
       Player player = find(chara);
       if(player == null){
         return "Tidak ada " + chara;
       }
       jawaban = player.getType() + " " + player.getName() + "\nHP: " + player.getHp() + "\n" + player.getAlive() + "\n" + diet2(chara); //Magician Magi\nHP: 150\nMasih hidup\Belum memakan siapa siapa
       return jawaban;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String jawaban = "";
        Boolean cekPertama  = false;
        for (Player i : player){
          if(cekPertama) {
            jawaban += "\n";
          }
          cekPertama = true;
          jawaban = jawaban + status(i.getName());
        }
        return jawaban;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player player = find(chara);
        if(player == null){
          return "Tidak ada " + chara;
        }
        String jawaban = "";
        ArrayList<Player> eaten = player.getEaten();
        for (Player i : eaten){
          jawaban += i.getType() + " " + i.getName();
        }
        if(jawaban.length() == 0){
          jawaban = "Belum memakan siapa siapa";
        }
        return jawaban;
    }

    public String diet2(String chara){
        Player player = find(chara);
        String jawaban = "Memakan ";
        ArrayList<Player> eaten = player.getEaten();
        for (Player i : eaten){
          jawaban += i.getType() + " " + i.getName();
        }
        if(jawaban.equals("Memakan ")){
          jawaban = "Belum memakan siapa siapa";
        }
        return jawaban;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String jawaban = "";
        for (Player i : player){
          jawaban += diet(i.getName());
        }
        return jawaban;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player player1 = find(meName),player2 = find(enemyName);
        if(player1 == null || player2 == null){
          return "Tidak ada " + meName + " atau " + enemyName;
        }
        if(player1.getIsDead()){
          return meName + " tidak bisa menyerang " + enemyName;
        }
        return player1.attack(player2);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player player1 = find(meName),player2 = find(enemyName);
        if(player1 == null || player2 == null){
          return "Tidak ada " + meName + " atau " + enemyName;
        }
        if(player1.getIsDead()){
          return meName + " tidak bisa membakar " + enemyName;
        }
        return player1.burn(player2);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
       String jawaban;
       Player player1 = find(meName),player2 = find(enemyName);
       if(player1 == null || player2 == null){
         return "Tidak ada " + meName + " atau " + enemyName;
       }
       if(player1.getIsDead()){
         return meName + " tidak bisa memakan " + enemyName;
       }
       if(player1.canEat(player2)){
          int hp = player1.getHp();
          hp += 15;
          player1.setHp(hp);
          jawaban = player1.getName() + " memakan "  + player2.getName() + "\nNyawa "+player1.getName()+" kini " + hp;
          player1.addEaten(player2);
          remove(enemyName);
        }
        else{
          jawaban = meName + " tidak bisa memakan "+ enemyName;
        }

        return jawaban;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan `mons`ter, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player player = find(meName);
        if(player == null){
            return "Tidak ada " + meName;
        }
        else if(!(player.getType().equals("Monster"))){
          return meName + " tidak bisa berteriak";
        }
        else {
          return player.roar();
        }
    }
}
