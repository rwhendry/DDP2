/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates;
	private int indeks;
	private boolean isBingo,hasRestart;
	private String nama;

	public BingoCard(Number[][] numbers, Number[] numberStates, String nama) {
		this.numbers = numbers;
		this.hasRestart = false;
		this.numberStates = numberStates;
		this.isBingo = false;
		this.indeks = 0;
		this.nama = nama;
	}

	public int getIndeks(){
		return indeks;
	}

	public void setIndeks(int indeks){
			this.indeks = indeks;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public Boolean findNumberStates(int number){
		Number[] temp = this.getNumberStates();
		for(int i=0;i<indeks;i++){
			if(temp[i].getValue() == number){
				return true;
			}
		}
		return false;
	}

	public String findNumber(int number){
		Number[][] temp = this.getNumbers();
		int indeksx = -1,indeksy = -1;
		String jaw = "";
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				if(temp[i][j].getValue() == number) {
					indeksx = i;
					indeksy = j;
				}
			}
		}
		if(indeksx == -1) return this.nama + ": Kartu tidak memiliki angka " + number;
		else{
			if(findNumberStates(number)){
				return this.nama + ": " + number+" sebelumnya sudah tersilang";
			}
			else{
				int inde = this.getIndeks();
				numberStates[inde] = new Number(number,indeksx,indeksy);
				inde++;
				this.setIndeks(inde);
				this.isBingo = cekBingo();
				return this.nama + ": " + number + " tersilang";
			}
		}
	}

	public String markNum(int num){
		return findNumber(num);
	}

	public Boolean cekBingo(){
		Number [][] temp = this.getNumbers();
		for(int i=0;i<5;i++){
			int cnt = 0;
			for(int j=0;j<5;j++){
				int num = temp[i][j].getValue();
				if(this.findNumberStates(num)) cnt++;
			}
			if(cnt == 5) {
				this.isBingo = true;
				return true;
			}
		}
		for(int i=0;i<5;i++){
			int cnt = 0;
			for(int j=0;j<5;j++){
				int num = temp[j][i].getValue();
				if(this.findNumberStates(num)) cnt++;
			}
			if(cnt == 5) {
				this.isBingo = true;
				return true;
			}
		}
		int cnt = 0,cnt2 = 0;
		for(int i=0;i<5;i++){
			int num = temp[i][i].getValue();
			if(this.findNumberStates(num)) cnt++;
			num = temp[i][4-i].getValue();
			if(this.findNumberStates(num)) cnt2++;
		}
		if(cnt == 5 || cnt2 == 5) {
			this.isBingo = true;
			return true;
		}
		return false;
	}

	public String info(){
		String jaw = this.nama + "\n";
		Number[][] temp = this.getNumbers();
		for(int i=0;i<5;i++){
			String tmp="| ";
			for(int j=0;j<5;j++){
				int number = temp[i][j].getValue();
				if(findNumberStates(number)){
					tmp = tmp +"X  ";
				}
				else tmp = tmp + Integer.toString(number)+ ' ';
				tmp = tmp + '|';
				if(j != 4) tmp = tmp + ' ';
			}
			if(i != 4)
			tmp = tmp + "\n";
			jaw = jaw + tmp;
		}
		return jaw;
	}

	public String restart(){
		if(this.hasRestart){
			return this.nama + " sudah pernah mengajukan RESTART";
		}
	 	Number temp[] = this.getNumberStates();
		for(int i=0;i<25;i++){
			temp[i] = new Number(100,-1,-1);
		}
		int inde = this.getIndeks();
		inde = 0;
		this.setIndeks(inde);
		this.setNumberStates(temp);
		this.hasRestart = true;
		return "Mulligan!";
	}
}
