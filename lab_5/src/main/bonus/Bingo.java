import java.util.Scanner;

public class Bingo{
  public static int ans = 0,faktorial = 1,answer = 0,N;
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    String b = input.nextLine();
    String a[] = b.split(" ");
    N = Integer.parseInt(a[0]);
    Number arr[][][] = new Number[N][5][5];
    String nama[] = new String[N];
    for(int i=0;i<N;i++){
      nama[i] = a[i+1];
    }
    for(int k=0;k<N;k++){
      for(int i=0;i<5;i++){
        String in = input.nextLine();
        String temp[] = in.split(" ");
        for(int j=0;j<5;j++) arr[k][i][j] = new Number(Integer.parseInt(temp[j]),i,j);
      }
    }
    Number kosong[] = new Number[25];
    for(int i=0;i<25;i++) kosong[i] = new Number(100,-1,-1);
    BingoCard Player[] = new BingoCard[N];
    for(int i=0;i<N;i++){
      Player[i] = new BingoCard(arr[i],kosong,nama[i]);
    }
    //BingoCard player = new BingoCard(arr,kosong);
    while(true){
        Boolean bingo = false;
        String in = input.nextLine();
        String temp[] = in.split(" ");
        if(temp[0].equals("MARK")){
          for(int i=0;i<N;i++)
          {
            System.out.println(Player[i].markNum(Integer.parseInt(temp[1])));
            if(Player[i].isBingo()){
              System.out.println(Player[i].info());
              bingo = true;
            }
          }
        }
        else if(temp[0].equals("INFO")){
          int indeks = -1;
          for(int i=0;i<N;i++) if(nama[i].equals(temp[1])) indeks = i;
          System.out.println(Player[indeks].info());
        }
        else if(temp[0].equals("RESTART")){
          int indeks = -1;
          for(int i=0;i<N;i++) if(nama[i].equals(temp[1])) indeks = i;
          System.out.println(Player[indeks].restart());
        }
        else{
          System.out.println("Incorrect Command");
        }
        if(bingo) break;
    }
    /*System.out.println("BINGO");
    player.info();*/
  }
}
