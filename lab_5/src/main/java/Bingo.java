import java.util.Scanner;

public class Bingo{
  public static int ans = 0,faktorial = 1,answer = 0;
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    Number arr[][] = new Number[5][5];
    for(int i=0;i<5;i++){
      String in = input.nextLine();
      String temp[] = in.split(" ");
      for(int j=0;j<5;j++) arr[i][j] = new Number(Integer.parseInt(temp[j]),i,j);
    }
    Number kosong[] = new Number[25];
    for(int i=0;i<25;i++) kosong[i] = new Number(100,-1,-1);
    BingoCard player = new BingoCard(arr,kosong);
    while(!player.cekBingo()){
        String in = input.nextLine();
        String temp[] = in.split(" ");
        if(temp[0].equals("MARK")){
          System.out.println(player.markNum(Integer.parseInt(temp[1])));
        }
        else if(temp[0].equals("INFO")){
          System.out.println(player.info());
        }
        else if(temp[0].equals("RESTART")){
          System.out.println(player.restart());
        }
        else{
          System.out.println("Incorrect Command");
        }

    }
    System.out.println("BINGO");
    player.info();
  }
}
