import java.util.Scanner;
import karyawan.intern.*;
import karyawan.manager.*;
import karyawan.staff.*;
import karyawan.*;
import java.util.ArrayList;

class Lab8 {

    static ArrayList<Karyawan> listKaryawan= new ArrayList<Karyawan>();
    static Scanner input = new Scanner(System.in);

    public static Karyawan findKaryawan(String nama){
        for(Karyawan karyawan : listKaryawan){
        //  System.out.println(karyawan.getNama());
          if(karyawan.getNama().equals(nama)){
            return karyawan;
          }
        }
        return null;
    }

    public static void readCommand(){
      String inpu[] = input.nextLine().split(" ");
      String command = inpu[0];
    //  System.out.println(command + "aa");
      if(command.equals("TAMBAH_KARYAWAN")){
        if(listKaryawan.size() > 10000){
          System.out.println("Kalo misal ampe ini kepanggil, niat banget Kak");
          return;
        }
        String tipe = inpu[1];
        String nama = inpu[2];
      //  System.out.println(tipe + " " + nama);
        int gaji = Integer.parseInt(inpu[3]);
        Karyawan karyawan;
        if(tipe.equals("Intern")){
          karyawan = new Intern(nama,gaji,tipe);
        }
        else if(tipe.equals("Manager")){
          karyawan = new Manager(nama,gaji,tipe);
        }
        else{
          karyawan = new Staff(nama,gaji,tipe);
        }
        System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
        listKaryawan.add(karyawan);
      }
      else if(command.equals("STATUS")){
        String nama = inpu[1];
        Karyawan karyawan = findKaryawan(nama);
        if(karyawan == null){
          System.out.println("Karyawan tidak ditemukan");
        }
        else{
          System.out.println(nama + " " + karyawan.getGajiPerBulan());
        }
      }
      else if(command.equals("TAMBAH_BAWAHAN")){
        String namaMenambahkan = inpu[1],namaDitambahkan = inpu[2];
        Karyawan karyawan1 = findKaryawan(namaMenambahkan);
        Karyawan karyawan2 = findKaryawan(namaDitambahkan);
        if(karyawan1 == null || karyawan2 == null){
          System.out.println("Nama tidak berhasil ditemukan");
        }
        else if(!karyawan1.bisaMenambahkan(karyawan2)){
          System.out.println("Anda tidak layak memiliki bawahan");
        }
        else if(karyawan1.cekBawahan(karyawan2)){
          System.out.println("Karyawan " + namaDitambahkan + " telah menjadi bawahan " + namaMenambahkan);
        }
        else {
          System.out.println("Karyawan "+ namaDitambahkan + " berhasil ditambahkan menjadi bawahan " + namaMenambahkan);
        }
      }
      else if(command.equals("GAJIAN")){
        System.out.println("Semua karyawan telah diberikan gaji");
        for (int i = 0;i < listKaryawan.size();i++){
          Karyawan karyawan = listKaryawan.get(i);
          karyawan.addGaji();
          if(karyawan.cekPromosi()){
            Karyawan karyawanBaru = (Manager) karyawan;
            listKaryawan.set(i,karyawanBaru);
          }
        }
      }
    }

    public static void main(String[] args) {
      int batasGaji;
      String inpu = input.nextLine();
      batasGaji = Integer.parseInt(inpu);
      Karyawan.setBatasGaji(batasGaji);
      while(true){
        readCommand();
      }
    }
}
