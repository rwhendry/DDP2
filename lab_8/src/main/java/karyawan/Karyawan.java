package karyawan;
import java.util.ArrayList;

public abstract class Karyawan{
    private static int batasGaji;
    private int gajiPerBulan,gaji,jumlahMendapatkanGaji;
    private String nama,tipe;
    private ArrayList<Karyawan> listBawahan = new ArrayList<Karyawan>();


    public Karyawan(String nama,int gaji,String tipe){
      this.nama = nama;
      this.gajiPerBulan = gaji;
      this.tipe = tipe;
      this.gaji = 0;
      this.jumlahMendapatkanGaji = 0;
    }

    public static void setBatasGaji(int batasGajiBaru){
      batasGaji = batasGajiBaru;
    }

    public String getNama(){
      return nama;
    }

    public int getGaji(){
      return gaji;
    }

    public int getGajiPerBulan(){
      return gajiPerBulan;
    }

    public String getTipe(){
      return tipe;
    }

    public Boolean bisaMenambahkan(Karyawan karyawan){
      String tipe2 = karyawan.getTipe();
      if(listBawahan.size() == 10){
        return false;
      }
      if(tipe.equals("MANAGER")){
        if(tipe2.equals("STAFF") || tipe2.equals("INTERN") ){
          return true;
        }
      }
      else if(tipe.equals("STAFF")){
        if(tipe2.equals("INTERN")){
          return true;
        }
      }
      return false;
    }

    public Boolean cekBawahan(Karyawan karyawan){
      for (Karyawan bawahan : listBawahan){
        if(karyawan == bawahan) return true;
      }
      return false;
    }

    public void menambahkanBawahan(Karyawan karyawan){
      listBawahan.add(karyawan);
    }

    public void cekNaikGaji(){
      if(jumlahMendapatkanGaji == 6){
        jumlahMendapatkanGaji = 0;
        int gajiBaru = gajiPerBulan * 110/ 100;
        System.out.println(nama+" mengalami kenaikan gaji sebesar 10% dari "+gajiPerBulan+" menjadi " + gajiBaru);
        gajiPerBulan = gajiBaru;
      }
    }

    public Boolean cekPromosi(){
      if(tipe.equals("STAFF") && gajiPerBulan > batasGaji){
        tipe = "MANAGER";
        System.out.println("Selamat, "+nama+" telah dipromosikan menjadi MANAGER");
        return true;
      }
      return false;
    }

    public void addGaji(){
      jumlahMendapatkanGaji++;
      cekNaikGaji();
      //cekPromosi();
    }
}
