package lab9.event;

import java.math.BigInteger;
import java.util.Date;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    private String startTime,endTime;
    private BigInteger cost;
    private Date startDate,endDate;

    // TODO: Make instance variables for representing beginning and end time of event

    // TODO: Make instance variable for cost per hour

    // TODO: Create constructor for Event class

    /**
    * Constructor
    * Initializes a event object with given name, start time, end time, cost, start date, end date
    * @param name the name of the event
    * @param startTime the starting time of the event in string
    * @param endTime the end time of the event in string
    * @param cost the cost of the event in string
    * @param startDate the starting time of the event in date with format yyyy-MM-dd_HH:mm:ss
    * @param endDate the ending tiem of the event in date with format yyyy-MM-dd_HH:mm:ss
    */
    public Event(String name,String startTime,String endTime,String cost,Date startDate,Date endDate){
      this.name = name;
      this.startTime = startTime;
      this.endTime = endTime;
      this.cost = new BigInteger(cost);
      this.startDate = startDate;
      this.endDate = endDate;
    }

    /**
    * Accessor for cost field.
    * @return cost of this event instance
    */
    public BigInteger getCost(){
      return cost;
    }

    /**
    * Accessor for start date field.
    * @return start date of this event instance
    */
    public Date getStartDate() {
      return startDate;
    }

    /**
    * Accessor for end date field.
    * @return end date of this event instance
    */
    public Date getEndDate(){
      return endDate;
    }

    /**
    * Accessor for name field.
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    @Override
    /**
    * return the string version of this event formated as
    * eventName
    * "Waktu mulai: " startTime with format dd_MM_yyyy, HH:mm:ss
    * "Waktu selesai: " endTime with format dd_MM_yyyy, HH:mm:ss
    * "Biaya kehadiran : " cost
    *
    * @return list of events this user plans to attend
    */
    public String toString(){
      String startTimeDate = "" + startTime.charAt(8) + startTime.charAt(9) + "-" + startTime.charAt(5)
      + startTime.charAt(6) + "-" + startTime.charAt(0) + startTime.charAt(1) + startTime.charAt(2) +
      startTime.charAt(3) + ", " + startTime.charAt(11) + startTime.charAt(12) + startTime.charAt(13) +
      startTime.charAt(14) + startTime.charAt(15) + startTime.charAt(16) + startTime.charAt(17) + startTime.charAt(18);
      String endTimeDate = "" + endTime.charAt(8) + endTime.charAt(9) + "-" + endTime.charAt(5) +
      endTime.charAt(6) + "-" + endTime.charAt(0) + endTime.charAt(1) + endTime.charAt(2) +
      endTime.charAt(3) + ", " + endTime.charAt(11) + endTime.charAt(12) + endTime.charAt(13) +
      endTime.charAt(14) + endTime.charAt(15) + endTime.charAt(16) + endTime.charAt(17) + endTime.charAt(18);
      String jawaban = name + "\nWaktu mulai: " + startTimeDate + "\nWaktu selesai: " + endTimeDate + "\nBiaya kehadiran: " + cost;
      return jawaban;
    }


    @Override
    /**
    * return the compare function of event
    *
    * @return -1 if eventNow before eventNext, 0 if equals and 1 if eventNow after eventNext
    */
    public int compareTo(Event eventNext) {
       Date nextDate =  eventNext.getStartDate();
       Date nowDate = this.getStartDate();
       int nilai;
       if(nowDate.before(nextDate)) nilai = -1;
       else if(nowDate.equals(nextDate)) nilai = 0;
       else nilai = 1;
       return nilai;

   }


    // TODO: Implement toString()

    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
