package lab9.user;

import lab9.event.Event;
import java.util.ArrayList;
import java.math.BigInteger;
import java.util.Collections;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;

    /** List of events this user plans to attend */
    private ArrayList<Event> events;

    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }

    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events. New event will be null if user cant
    * register to that event
    *
    * @param newEvent the new event that are going to be added if it is not null
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        if(newEvent == null) return false;
        events.add(newEvent);
        return true;
    }

    /**
    * compute the total cost of all event that user registered
    *
    * @return total cost of all event user registered
    */
    public BigInteger getTotalCost(){
      BigInteger costSementara = new BigInteger("0");
      for(Event event : events){
          costSementara = costSementara.add(event.getCost());
      }
      return costSementara;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> copy = new ArrayList<Event>(events);
        Collections.sort(copy);
        // TODO: Implement!
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)                                                                                                          (or... Google. Yeah that works too. OK.)
        return copy;
    }
}
