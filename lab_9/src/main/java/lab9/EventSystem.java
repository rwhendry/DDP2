package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.text.SimpleDateFormat;
import java.util.Date;


import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;

    /**
    * List of users
    */
    private ArrayList<User> users;

    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        Event event = findEvent(name);
        if(event == null){
            try{
              SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
              Date startDate = sdf.parse(startTimeStr);
              Date endDate = sdf.parse(endTimeStr);
              if(startDate.after(endDate)){
                return "Waktu yang diinputkan tidak valid!";
              }
              event = new Event(name,startTimeStr,endTimeStr,costPerHourStr,startDate,endDate);
              events.add(event);
              return "Event "+ name + " berhasil ditambahkan!";
            } catch(Exception e){

            }
        }
        else{
          return "Event " + name + " sudah ada!";
        }
        return "";
        // TODO: Implement!
        //return false;
    }

    public String addUser(String name)
    {
        User user = findUser(name);
        String answer;
        if(user == null){
          user = new User(name);
          users.add(user);
          return "User " + name + " berhasil ditambahkan!";
        }
        else{
          return "User " + name + " sudah ada!";
        }
    }

    public String registerToEvent(String userName, String eventName)
    {
        User user = findUser(userName);
        Event event = findEvent(eventName);
        if(user == null && event == null){
          return "Tidak ada pengguna dengan nama "+userName+" dan acara dengan nama "+eventName+"!";
        }
        else if(user == null){
          return "Tidak ada pengguna dengan nama "+userName+"!";
        }
        else if(event == null){
          return "Tidak ada acara dengan nama "+eventName+"!";
        }
        else{
          for(Event eventCek : user.getEvents()){
            Date startDateNow = event.getStartDate();
            Date endDateNow = event.getEndDate();
            Date startDateEvent = eventCek.getStartDate();
            Date endDateEvent = eventCek.getEndDate();
            if(!(startDateNow.equals(endDateEvent) || startDateNow.after(endDateEvent) || endDateNow.equals(startDateEvent) || endDateNow.before(startDateEvent))){
              user.addEvent(null);
              return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }
          }
          user.addEvent(event);
          return userName + " berencana menghadiri " + eventName + "!";
        }
    }

    public User findUser(String userName){
      for(User user: users){
        if(user.getName().equals(userName)) return user;
      }
      return null;
    }

    public Event findEvent(String eventName){
      for(Event event : events){
        if(event.getName().equals(eventName)) return event;
      }
      return null;
    }

    public Event getEvent(String eventName){
      Event event = findEvent(eventName);
      return event;
    }

    public User getUser(String userName){
      return findUser(userName);
    }
}
