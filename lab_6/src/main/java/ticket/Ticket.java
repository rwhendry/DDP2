package ticket;
import movie.*;
import java.util.ArrayList;

public class Ticket{
    private Movie film;
    private String hari;
    private Boolean jenis;
    private int harga;
    public Ticket(Movie film,String hari,Boolean jenis){
        this.film = film;
        this.hari = hari;
        this.jenis = jenis;
        this.harga = 0;
    }

    public Boolean getJenis(){
      return jenis;
    }

    public Movie getMovie(){
      return film;
    }

    public String getHari(){
      return hari;
    }

    public void setHarga(int harga){
        this.harga = harga;
    }

    public int getHarga(){
      return harga;
    }

    public Boolean equal(Ticket tiket){
      Boolean sama = true;
      sama = sama & (film.equal(tiket.film));
      sama = sama & (hari.equals(tiket.hari));
      sama = sama & (tiket.jenis == tiket.jenis);
      return sama;
    }
}
