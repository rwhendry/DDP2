package customer;
import java.util.ArrayList;
import theater.*;
import ticket.*;
import movie.*;

public class Customer{
    private String nama;
    private int umur,hargaTiketTerakhir;;
    Boolean used;
    Boolean jenisKelamin;
    ArrayList<Ticket> tiketTerbeli = new ArrayList<Ticket>();
    ArrayList<Boolean> tiketTerpakai = new ArrayList<Boolean>(),tiketDibalikkan = new ArrayList<Boolean>();

    public Customer(String nama,Boolean jenisKelamin,int umur){
          this.nama = nama;
          this.jenisKelamin = jenisKelamin;
          this.umur = umur;
          this.used = false;
    }

    public Ticket orderTicket(Theater teater,String judul,String hari, String jenis){
      ArrayList<Ticket> tiketTeater = teater.getTicket();
      Ticket tiket = null;
      Boolean valid = true;
      Boolean cek3D = false;
      String ratingFail = "";
      if(jenis.equals("3 Dimensi")) cek3D = true;
      for(Ticket tiketSekarang: tiketTeater){
          Movie film = tiketSekarang.getMovie();
          String judulFilm = film.getJudul();
          String hariFilm = tiketSekarang.getHari();
          Boolean jenisFilm = tiketSekarang.getJenis();
          if(judulFilm.equals(judul) && hariFilm.equals(hari) && cek3D == jenisFilm){
            tiket = tiketSekarang;
            String rating = film.getRating();
            if((umur < 13 && (rating.equals("Remaja") || rating.equals("Dewasa")) || (umur < 17 && rating.equals("Dewasa")))) {
              valid = false;
              ratingFail = rating;
            }
          }
      }
      if(tiket != null){
        if(valid){
            int harga = 60000;
            if(hari.equals("Sabtu") || hari.equals("Minggu")) harga = harga + 40000;
            if(cek3D) harga = 6*harga/5;
            hargaTiketTerakhir = harga;
            int kas = teater.getSaldoKas();
            kas = kas + harga;
            teater.setSaldoKas(kas);
            System.out.println(nama + " telah membeli tiket "+judul+" jenis "+jenis+" di "+teater.getNama()+" pada hari "+ hari+" seharga Rp. "+harga);
            tiketTerbeli.add(tiket);
            tiketTerpakai.add(false);
            tiketDibalikkan.add(false);
            tiket.setHarga(harga);
        }
        else{
            System.out.println(nama+" masih belum cukup umur untuk menonton "+judul+" dengan rating " + ratingFail);
        }
      }
      else{
          System.out.println("Tiket untuk film "+judul+" jenis "+jenis+" dengan jadwal "+hari+" tidak tersedia di "+teater.getNama());
      }

      return tiket;
    }

    public void findMovie(Theater teater,String judul){
      Movie[] listFilm = teater.getMovie();
      for(int i=0;i<listFilm.length;i++){
        Movie film = listFilm[i];
        String namaFilm = film.getJudul();
        if(namaFilm.equals(judul)){
          film.printInfo();
          return;
        }
      }
      System.out.println("Film "+judul+" yang dicari "+ nama + " tidak ada di bioskop "+teater.getNama());
    }

    public void watchMovie(Ticket tiket){
      Ticket tiketTerakhir = null;
      boolean tiketDipake = false;;
      int harga = 0;
      int indeksTiket = 0;
      for(int i=tiketTerbeli.size()-1;i>=0;i--){
        Ticket tiketSekarang = tiketTerbeli.get(i);
        if(tiket.equal(tiketSekarang)){
          tiketTerpakai.set(i,true);
          System.out.println(nama+" telah menonton film "+(tiket.getMovie().getJudul()));
          return;
        }
      }
    }

    public void cancelTicket(Theater theater){
      Ticket tiketTerakhir = null;
      boolean tiketDipake = false;;
      int harga = 0;
      int indeksTiket = 0;
      for(int i=tiketTerbeli.size()-1;i>=0;i--){
        if(tiketDibalikkan.get(i) == false){
          tiketTerakhir = tiketTerbeli.get(i);
          tiketDipake = tiketTerpakai.get(i);
          harga = tiketTerakhir.getHarga();
          indeksTiket = i;
          break;
        }
      }
      Boolean ada = theater.cekFilm(tiketTerakhir.getMovie());
    // System.out.println((tiketTerakhir.getMovie()).getJudul() + " " + nama);
      if(ada){
        if(tiketDipake){
          System.out.println("Tiket tidak bisa dikembalikan karena film "+(tiketTerakhir.getMovie()).getJudul()+" sudah ditonton oleh "+this.nama);
        }
        else{
          int totalKas = theater.getSaldoKas();
          if(totalKas >= harga) {
            System.out.println("Tiket film "+(tiketTerakhir.getMovie()).getJudul()+" dengan waktu tayang "+tiketTerakhir.getHari()+" jenis "+ (tiketTerakhir.getJenis() ? "3 Dimensi" : "Biasa")+ " dikembalikan ke bioskop "+theater.getNama());
            totalKas -= harga;
            theater.setSaldoKas(totalKas);
            tiketDibalikkan.set(indeksTiket,true);
          }
          else{
            System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop "+theater.getNama()+" lagi tekor...");
          }
        }
      }
      else{
          System.out.println("Maaf tiket tidak bisa dikembalikan, "+ (tiketTerakhir.getMovie()).getJudul()+" tidak tersedia dalam "+theater.getNama());
      }
    }
}
