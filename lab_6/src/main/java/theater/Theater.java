package theater;
import java.util.ArrayList;
import movie.*;
import ticket.*;

public class Theater{
    private String nama;
    private int saldoKas;
    private ArrayList<Ticket> ticket;
    private Movie[] movie;
    public Theater(String nama,int saldoKas,ArrayList<Ticket> ticket,Movie[] movie){
          this.nama = nama;
          this.saldoKas = saldoKas;
          this.movie = movie;
          this.ticket = ticket;
    }

    public void printInfo(){
      System.out.println("------------------------------------------------------------------");
      System.out.println("Bioskop                 : " + this.nama);
      System.out.println("Saldo Kas               : " + this.saldoKas);
      System.out.println("Jumlah tiket tersedia   : " + ticket.size());
      System.out.print("Daftar Film tersedia    : ");
      for(int i=0;i<this.movie.length;i++){
        if(i != 0) System.out.print(", ");
        System.out.print(movie[i].getJudul());
      }
      System.out.println();
      System.out.println("------------------------------------------------------------------");
    }

    public Movie[] getMovie(){
      return movie;
    }

    public ArrayList<Ticket> getTicket(){
      return ticket;
    }

    public String getNama(){
      return nama;
    }

    public int getSaldoKas(){
      return saldoKas;
    }

    public void setSaldoKas(int saldoKas){
      this.saldoKas = saldoKas;
    }

    public static void printTotalRevenueEarned(Theater[] teater){
      int totalPendapatan = 0;
      Theater[] listTheater = teater;
      for(int i=0;i<listTheater.length;i++){
        Theater theater = listTheater[i];
        totalPendapatan += theater.getSaldoKas();
      }
      System.out.println("Total uang yang dimiliki Koh Mas : Rp. "+totalPendapatan);
      System.out.println("------------------------------------------------------------------");
      for(int i=0;i<listTheater.length;i++){
        Theater theater = listTheater[i];
        System.out.println("Bioskop         : "+theater.getNama());
        System.out.println("Saldo Kas       : Rp. "+theater.getSaldoKas());
      }
      System.out.println("------------------------------------------------------------------");
    }


    public Boolean cekFilm(Movie tiketCek){
      for(int i=0;i<movie.length;i++){
        if(movie[i].equal(tiketCek)) return true;
      }
      return false;
    }
}
